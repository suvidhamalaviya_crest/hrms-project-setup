package com.crest.hrms;

import android.app.Application;

import androidx.appcompat.app.AppCompatDelegate;

import com.crest.hrms.injection.component.DaggerNetworkComponent;
import com.crest.hrms.injection.component.DaggerServiceComponent;
import com.crest.hrms.injection.component.NetworkComponent;
import com.crest.hrms.injection.component.ServiceComponent;
import com.crest.hrms.injection.module.AppModule;
import com.crest.hrms.injection.module.NetworkModule;
import com.crest.hrms.util.AppConstant;
import com.crest.hrms.util.PreferenceHelper;
import com.crest.hrms.util.StringHelper;
import com.crest.hrms.util.VariantConfig;

public class HRMSApp extends Application {
    public static NetworkComponent networkComponent;
    public static ServiceComponent serviceComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        setupSplunkRUM();
        // Enable Vector Image
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        initDagger();

    }

    /**
     * Init dagger2 network and service component
     */
    public void initDagger() {
        // Dagger 2 Network Component
        networkComponent = DaggerNetworkComponent.builder()
                .networkModule(new NetworkModule(VariantConfig.getServerBaseUrl()))
                .appModule(new AppModule(this))
                .build();

        // Dagger 2 Service Component
        serviceComponent = DaggerServiceComponent.builder()
                .networkComponent(networkComponent)
                .build();
    }

    /**
     * Setup Splunk RUM Library
     */
    private void setupSplunkRUM() {

        String token = PreferenceHelper.getValue(this, AppConstant.SharedPrefKey.TOKEN, String.class, "");
        String environmentName = PreferenceHelper.getValue(this, AppConstant.SharedPrefKey.ENVIRONMENT_NAME,
                String.class, "");
        String appName = PreferenceHelper.getValue(this, AppConstant.SharedPrefKey.APP_NAME, String.class, "");
        String realM = PreferenceHelper.getValue(this, AppConstant.SharedPrefKey.REAL_M,
                String.class, "");

        if (StringHelper.isEmpty(token)) {
            //token = getString(R.string.rum_access_token);
        }

        if (StringHelper.isEmpty(environmentName)) {
           // environmentName = getString(R.string.rum_environment);
        }

        if (StringHelper.isEmpty(realM)) {
            //realM = getResources().getString(R.string.rum_realm);
        }

        if (StringHelper.isEmpty(appName)) {
            appName = getString(R.string.app_name);
        }
    }

    // Get Service component
    public static ServiceComponent getServiceComponent() {
        return serviceComponent;
    }

}
