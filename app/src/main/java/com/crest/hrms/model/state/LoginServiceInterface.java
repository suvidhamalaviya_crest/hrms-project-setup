package com.crest.hrms.model.state;

import com.crest.hrms.model.entity.request.LoginRequest;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface LoginServiceInterface {

    @POST("/login")
    Observable<ResponseBody> performLogin(@Body LoginRequest loginRequest);

}
