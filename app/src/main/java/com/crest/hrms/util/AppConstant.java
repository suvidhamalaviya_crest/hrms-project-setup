package com.crest.hrms.util;


public final class AppConstant {

    // Defined all common constants
    public static final String ERROR_UNKNOWN = "ERR0001";
    public static final String ERROR_INTERNET = "ERR_INTERNET";
    public static final int SPLASH_SCREEN_DURATION = 3000;

    public static final String SUCCESS_MESSAGE = "Logged In Successfully";
    public static final String USER_NAME_REQUIRED = "User Name Required";
    public static final String PASSWORD_REQUIRED = "Password Required";

    @SuppressWarnings("ALL")
    public static final String PRODUCT_JSON_FILE_NAME = "product.json";


    // Defind all the dialog identifier constants
    @SuppressWarnings("ALL")
    public static final class DialogIdentifier {
        public static final int EMPTY_CART = 2;
        public static final int EXIT = 3;
        public static final int INTERNET_DIALOG = 4;
        public static final int CHECK_OUT_DIALOG = 5;
    }

    // Defined all the shared pref key constants
    public static final class SharedPrefKey {
        public static final String TOKEN = "TOKEN";
        public static final String ENVIRONMENT_NAME = "ENVIRONMENT_NAME";
        public static final String REAL_M = "REAL_M";
        public static final String APP_NAME = "APP_NAME";
    }

    @SuppressWarnings("ALL")
    public static final class ErrorType {
        public static final String ERR_4XX = "4xx";
        public static final String ERR_5XX = "5xx";
        public static final String ERR_EXCEPTION = "Exception";
        public static final String ERR_CRASH = "crash";
        public static final String ERR_FREEZE = "Freeze";
        public static final String ERR_ANR = "ANR";
    }

    @SuppressWarnings("ALL")
    public static final class ErrorAction {
        public static final String ACTION_CART = "cart";
        public static final String ACTION_ADD_PRODUCT = "Add Product";
        public static final String ACTION_VIEW = "view";
    }


}
