package com.crest.hrms.util;

public class VariantConfig {

    private static String serverBaseUrl = "http://122.169.101.20:8443";

    public static void setServerBaseUrl(String baseUrl) {
        serverBaseUrl = baseUrl;
    }

    public static String getServerBaseUrl() {
        return serverBaseUrl;
    }
}
