package com.crest.hrms.callback;

import com.crest.hrms.network.RetrofitException;

@SuppressWarnings("ALL")
public interface ViewListener {


    void showProgress();

    void hideProgress();

    void showApiError(RetrofitException retrofitException, String errorCode);

    boolean isNetworkAvailable();
}
