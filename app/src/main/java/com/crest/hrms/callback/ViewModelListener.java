package com.crest.hrms.callback;

@SuppressWarnings("ALL")
public interface ViewModelListener<T> {

    void createView(T view);

    void destroyView();
}
