package com.crest.hrms.injection.scope;

import javax.inject.Scope;

@Scope
public @interface UserScope {
}
