package com.crest.hrms.injection.module;

import com.crest.hrms.injection.scope.UserScope;
import com.crest.hrms.model.state.LoginServiceInterface;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class LoginModule {

    @UserScope
    @Provides
    public LoginServiceInterface provideLoginService(Retrofit retrofit){
        return retrofit.create(LoginServiceInterface.class);
    }

}
