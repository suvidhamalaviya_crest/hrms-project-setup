package com.crest.hrms.injection.component;


import com.crest.hrms.injection.module.LoginModule;
import com.crest.hrms.injection.scope.UserScope;
import com.crest.hrms.view.home.viewModel.LoginViewModel;

import dagger.Component;

@UserScope
@Component(dependencies = NetworkComponent.class,
        modules = { LoginModule.class })

@SuppressWarnings("ALL")
public interface ServiceComponent {
    // for authentication view model

    // Reference Code
    // for checkout model
    void inject(LoginViewModel loginViewModel);
}
