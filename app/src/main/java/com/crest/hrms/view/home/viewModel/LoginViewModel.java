package com.crest.hrms.view.home.viewModel;
import android.view.View;

import androidx.lifecycle.MutableLiveData;

import com.crest.hrms.HRMSApp;
import com.crest.hrms.model.entity.request.LoginRequest;
import com.crest.hrms.model.state.LoginServiceInterface;
import com.crest.hrms.network.RXRetroManager;
import com.crest.hrms.network.RetrofitException;
import com.crest.hrms.util.AppConstant;
import com.crest.hrms.util.ResourceProvider;
import com.crest.hrms.view.base.viewModel.BaseViewModel;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.ResponseBody;


public class LoginViewModel extends BaseViewModel {

    @SuppressWarnings("ALL")
    @Inject
    LoginServiceInterface loginServiceInterface;
    private MutableLiveData<ResponseBody> baseResponse;

    public final MutableLiveData<String> username = new MutableLiveData<>();
    public final MutableLiveData<String> password = new MutableLiveData<>();

    private MutableLiveData<LoginRequest> loginRequest;

    public LoginViewModel(ResourceProvider resourceProvider) {
        HRMSApp.getServiceComponent().inject(this);
    }

    public MutableLiveData<LoginRequest> getLoginRequest() {
        if (loginRequest == null) {
            loginRequest = new MutableLiveData<>();
        }
        return loginRequest;
    }

    public void setLoginRequest(MutableLiveData<LoginRequest> loginRequest) {
        this.loginRequest = loginRequest;
    }

    public void getLoginResponse() {
        if (view != null && view.isNetworkAvailable()) {
            new RXRetroManager<ResponseBody>() {
                @Override
                protected void onSuccess(ResponseBody response) {
                    if (view != null) {
                        baseResponse.postValue(response);
                    }
                }

                @Override
                protected void onFailure(RetrofitException retrofitException, String errorCode) {
                    super.onFailure(retrofitException, errorCode);
                    if (view != null) {
                        view.showApiError(retrofitException, errorCode);
                    }
                }
            }.rxSingleCall(loginServiceInterface.performLogin(loginRequest.getValue()));
        } else {
            if (view != null) {
                RetrofitException retrofitException = RetrofitException.networkError(new IOException(""));
                view.showApiError(retrofitException, AppConstant.ERROR_INTERNET);
            }
        }
    }

    public void onLoginClick(View view) {

        LoginRequest loginUser = new LoginRequest(username.getValue(), password.getValue());

        loginRequest.setValue(loginUser);

        getLoginResponse();

    }

    public MutableLiveData<ResponseBody> getBaseResponse() {
        if (baseResponse == null) {
            baseResponse = new MutableLiveData<>();
        }
        return baseResponse;
    }

}
