package com.crest.hrms.view.base.viewModel;

import android.os.Handler;

import com.crest.hrms.callback.BaseViewModelListener;
import com.crest.hrms.callback.ViewListener;


/**
 * Base class for all ViewModels.  Reason for this class is that all viewModels need a
 * reference
 * to the UI Thread.
 */
@SuppressWarnings("ALL")
abstract public class BaseViewModel extends BaseViewModelListener<ViewListener> {
    private Handler mUiThreadHandler;

    public void onCreate(Handler handler) {
        mUiThreadHandler = handler;
    }

    public void onDestroy() {
        mUiThreadHandler = null;
    }

    public Handler getUiThreadHandler() {
        return mUiThreadHandler;
    }

    public void setUiThreadHandler(Handler mUiThreadHandler) {
        this.mUiThreadHandler = mUiThreadHandler;
    }
}
