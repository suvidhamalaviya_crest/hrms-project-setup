package com.crest.hrms.view.base.fragment;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.crest.hrms.R;
import com.crest.hrms.callback.DialogButtonClickListener;
import com.crest.hrms.callback.ViewListener;
import com.crest.hrms.network.RetrofitException;
import com.crest.hrms.util.AlertDialogHelper;
import com.crest.hrms.util.AppConstant;
import com.crest.hrms.util.AppUtils;
import com.crest.hrms.util.StringHelper;

// NEED MODIFICATION
public class BaseFragment extends Fragment implements ViewListener, DialogButtonClickListener {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressWarnings("unused")
    @Override
    public void showProgress() {
    }

    @SuppressWarnings("unused")
    @Override
    public void hideProgress() {
    }

    @Override
    public boolean isNetworkAvailable() {
        if (getContext() != null) {
            return AppUtils.isNetworkAvailable(getContext());
        } else {
            return AppUtils.isNetworkAvailable(requireActivity());
        }
    }

    @SuppressWarnings("unused")
    @Override
    public void showApiError(RetrofitException retrofitException, String errorCode) {
        if (getActivity() != null) {
            if (errorCode.equalsIgnoreCase(AppConstant.ERROR_INTERNET)) {
                if(retrofitException != null && StringHelper.isNotEmpty(retrofitException.getMessage())){
                    /*isCart = retrofitException.getMessage().equalsIgnoreCase(getString(R.string.rum_event_add_to_cart));
                    is4xx = retrofitException.getMessage().equalsIgnoreCase(getString(R.string.method_not_found));
                    is5xx = retrofitException.getMessage().equalsIgnoreCase(getString(R.string.http_error));
                    isSlowAPI = retrofitException.getMessage().equalsIgnoreCase(getString(R.string.slow_api));*/
                    AlertDialogHelper.showDialog(getActivity(), null, getString(R.string.error_network)
                            , getString(R.string.ok), getString(R.string.retry), false,
                            this, AppConstant.DialogIdentifier.INTERNET_DIALOG);
                }
            } else {
                AppUtils.handleApiError(getActivity(), retrofitException);
            }
        }
    }


    @Override
    public void onPositiveButtonClicked(int dialogIdentifier) {
    }

    @Override
    public void onNegativeButtonClicked(int dialogIdentifier) {
        if (dialogIdentifier == AppConstant.DialogIdentifier.INTERNET_DIALOG) {
            /*if (this instanceof ProductListFragment) {
                ((ProductListFragment) this).getProductViewModel().getProductList();
            } else if (this instanceof ProductDetailsFragment) {
                if (isCart) {
                    ((ProductDetailsFragment) this).getProductViewModel().addToCart(String.valueOf(((ProductDetailsFragment) this).getProductDetails().getQuantity()),
                            ((ProductDetailsFragment) this).getProductDetails().getId());
                } else {
                    ((ProductDetailsFragment) this).getProductViewModel().getProductDetail(((ProductDetailsFragment) this).getProductDetails().getId());
                }


                if (is4xx) {
                    ((ProductDetailsFragment) this).getEventViewModel().generateHttpNotFound();
                }

                if (is5xx) {
                    ((ProductDetailsFragment) this).getEventViewModel().generateHttpError(((ProductDetailsFragment) this).getProductDetails().getId(),1);
                }

            } else if (this instanceof ShoppingCartFragment) {
                if (isSlowAPI) {
                    ((ShoppingCartFragment) this).getViewModel().slowApiResponse();
                } else {
                    ((ShoppingCartFragment) this).getProductViewModel().getCartItems();
                }
            } else if (this instanceof EventGenerationFragment) {
                if (is4xx) {
                    ((EventGenerationFragment) this).getViewModel().generateHttpNotFound();
                }

                if (is5xx) {
                    ((EventGenerationFragment) this).getViewModel().generateHttpError("",0);
                }

                if (isSlowAPI) {
                    ((EventGenerationFragment) this).getViewModel().slowApiResponse();
                }
            }*/
        }
    }
}
