package com.crest.hrms.view.splash;

import androidx.databinding.DataBindingUtil;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;

import com.crest.hrms.R;
import com.crest.hrms.util.AppConstant;
import com.crest.hrms.view.base.activity.BaseActivity;
import com.crest.hrms.view.home.LoginActivity;

@SuppressLint("CustomSplashScreen")
public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataBindingUtil.setContentView(this, R.layout.activity_splash);
        new Handler().postDelayed(() -> moveActivity(this, LoginActivity.class, true, true), AppConstant.SPLASH_SCREEN_DURATION);
    }
}