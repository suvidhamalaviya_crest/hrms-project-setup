package com.crest.hrms.view.base.viewModel;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.crest.hrms.util.ResourceProvider;
import com.crest.hrms.view.home.viewModel.LoginViewModel;

// NEED MODIFICATION
public class ViewModelFactory implements ViewModelProvider.Factory {
    private final ResourceProvider resourceProvider;


    public ViewModelFactory(ResourceProvider resourceProvider) {
        this.resourceProvider = resourceProvider;
    }

    @NonNull
    @SuppressWarnings("unchecked")
    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(LoginViewModel.class)) {
            return (T) new LoginViewModel(resourceProvider);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}