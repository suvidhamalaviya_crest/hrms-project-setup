package com.crest.hrms.view.home;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.crest.hrms.R;
import com.crest.hrms.databinding.ActivityLoginBinding;
import com.crest.hrms.model.entity.response.LoginResponse;
import com.crest.hrms.util.AppConstant;
import com.crest.hrms.util.AppUtils;
import com.crest.hrms.util.ResourceProvider;
import com.crest.hrms.view.base.activity.BaseActivity;
import com.crest.hrms.view.base.viewModel.ViewModelFactory;
import com.crest.hrms.view.home.viewModel.LoginViewModel;
import com.google.gson.Gson;

import java.util.Objects;

import okhttp3.ResponseBody;

public class LoginActivity extends BaseActivity {

    private Context mContext;
    private ActivityLoginBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mContext = this;

        // Initialize data binding
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        // Configure ViewModel
        LoginViewModel loginViewModel = new ViewModelProvider(this, new ViewModelFactory(new ResourceProvider(getResources()))).get(LoginViewModel.class);
        binding.setViewModel(loginViewModel);
        loginViewModel.createView(this);
        binding.setLifecycleOwner(this);

        // handle api call response data
        loginViewModel.getBaseResponse()
                .observe(this,
                        handleResponse());

        loginViewModel.getLoginRequest().observe(this, loginUser -> {

            if (TextUtils.isEmpty(Objects.requireNonNull(loginUser).getUsername())) {
                AppUtils.showShortMessage(mContext,AppConstant.USER_NAME_REQUIRED);
                binding.edtUserName.requestFocus();
            }
            else if (TextUtils.isEmpty(Objects.requireNonNull(loginUser).getPassword())) {
                AppUtils.showShortMessage(mContext,AppConstant.PASSWORD_REQUIRED);
                binding.edtPassword.requestFocus();
            }
        });

    }

    /**
     * @return Handle product list API Response
     */
    private androidx.lifecycle.Observer<ResponseBody> handleResponse() {
        return response -> {
            try {
                Gson gson = new Gson();
                LoginResponse loginResponse = gson.fromJson(response.string(), LoginResponse.class);

                Log.e("Main Response: ", loginResponse.getEmail());
                String responseMessage = "Email : " + loginResponse.getEmail()
                        +"\nEmp Code : " + loginResponse.getEmpCode()
                        +"\nName : " + loginResponse.getName()
                        +"\nRole : " + loginResponse.getRole()
                        +"\nStatus : " + loginResponse.getStatus();
                new AlertDialog.Builder(mContext)
                        .setTitle(AppConstant.SUCCESS_MESSAGE)
                        .setMessage(responseMessage)
                        .setNegativeButton(android.R.string.ok, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            } catch (Exception e) {
                //AppUtils.handleRumException(e);
            }
        };
    }


}